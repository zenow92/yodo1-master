package com.zeetech.yodo1.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.yodo1.mas.Yodo1Mas;
import com.yodo1.mas.error.Yodo1MasError;
import com.yodo1.mas.event.Yodo1MasAdEvent;

public class ThroughUtilityIntigrationActivity extends AppCompatActivity {


    Yodo1Utils mYodo1Utils;
    Button bt_Interstitial, bt_Rewarded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ads_lay_activity);
bt_Interstitial=findViewById(R.id.interstitial_bt_id);
bt_Rewarded=findViewById(R.id.rewarded_bt_id);


 ////////////Initialization
        mYodo1Utils=new Yodo1Utils(ThroughUtilityIntigrationActivity.this);

//////Show Banner on Top or Bottom. For bottom, use "Yodo1Mas.BannerBottom"
        mYodo1Utils.showBanner(Yodo1Mas.BannerTop);




 ////////////interstitial Listener
        mYodo1Utils.setOnInterstitialClickListener(new Yodo1Mas.InterstitialListener() {
            @Override
            public void onAdOpened(@NonNull Yodo1MasAdEvent event) {

            }

            @Override
            public void onAdError(@NonNull Yodo1MasAdEvent event, @NonNull Yodo1MasError error) {

            }

            @Override
            public void onAdClosed(@NonNull Yodo1MasAdEvent event) {

                ////////do your stuff on ad close i,e go to next activity etc
                startActivity(new Intent(ThroughUtilityIntigrationActivity.this,SecondActivity.class));
            }
        });


  ///////////////////Rewarded Listener
        mYodo1Utils.setRewardedClickListener(new Yodo1Mas.RewardListener() {

            @Override
            public void onAdError(@NonNull Yodo1MasAdEvent event, @NonNull Yodo1MasError error) {
                super.onAdError(event, error);
            }

            @Override
            public void onAdOpened(@NonNull Yodo1MasAdEvent event) {
                super.onAdOpened(event);
            }

            @Override
            public void onAdvertRewardEarned(@NonNull Yodo1MasAdEvent event) {

            }

            @Override
            public void onAdClosed(@NonNull Yodo1MasAdEvent event) {
                super.onAdClosed(event);

                ////////do your stuff on ad close i,e go to next activity etc
                startActivity(new Intent(ThroughUtilityIntigrationActivity.this,SecondActivity.class));
            }

        });



        ////show interstitial on Button click
       bt_Interstitial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mYodo1Utils.isInterstitialLoaded())
             mYodo1Utils.showInterstitial();
            }
        });

  ////show Rewarded Ad
       bt_Rewarded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mYodo1Utils.isRewardedLoaded())
             mYodo1Utils.showRewarded();
            }
        });


    }


}