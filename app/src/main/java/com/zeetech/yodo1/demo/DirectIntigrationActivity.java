package com.zeetech.yodo1.demo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.yodo1.mas.Yodo1Mas;
import com.yodo1.mas.error.Yodo1MasError;
import com.yodo1.mas.event.Yodo1MasAdEvent;

public class DirectIntigrationActivity extends AppCompatActivity {


   static Yodo1Mas mYodo1Mas;
    Button bt_Interstitial, bt_Rewarded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ads_lay_activity);
bt_Interstitial=findViewById(R.id.interstitial_bt_id);
bt_Rewarded=findViewById(R.id.rewarded_bt_id);


////get instance of yodo1
        mYodo1Mas=Yodo1Mas.getInstance();

 ////////////Intialize sdk one Time only in App(i.e In main activity , not on all activities)
        mYodo1Mas.init(this, getResources().getString(R.string.yodo_app_key), new Yodo1Mas.InitListener() {
            @Override
            public void onMasInitSuccessful() {

            }

            @Override
            public void onMasInitFailed(@NonNull Yodo1MasError error) {
            }
        });

  //////show banner in onResume of activity for best practice

 ////////////interstitial Listener
        mYodo1Mas.setInterstitialListener(new Yodo1Mas.InterstitialListener() {
            @Override
            public void onAdOpened(@NonNull Yodo1MasAdEvent event) {

            }

            @Override
            public void onAdError(@NonNull Yodo1MasAdEvent event, @NonNull Yodo1MasError error) {

            }

            @Override
            public void onAdClosed(@NonNull Yodo1MasAdEvent event) {

                ////////do your stuff on ad close i,e go to next activity etc
                startActivity(new Intent(DirectIntigrationActivity.this,SecondActivity.class));
            }
        });


  ///////////////////Rewarded Listener
        mYodo1Mas.setRewardListener(new Yodo1Mas.RewardListener() {

            @Override
            public void onAdError(@NonNull Yodo1MasAdEvent event, @NonNull Yodo1MasError error) {
                super.onAdError(event, error);
            }

            @Override
            public void onAdOpened(@NonNull Yodo1MasAdEvent event) {
                super.onAdOpened(event);
            }

            @Override
            public void onAdvertRewardEarned(@NonNull Yodo1MasAdEvent event) {

            }

            @Override
            public void onAdClosed(@NonNull Yodo1MasAdEvent event) {
                super.onAdClosed(event);

                ////////do your stuff on ad close i,e go to next activity etc
                startActivity(new Intent(DirectIntigrationActivity.this,SecondActivity.class));
            }

        });



        ////show interstitial on Button click
       bt_Interstitial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mYodo1Mas.isInterstitialAdLoaded())
             mYodo1Mas.showInterstitialAd(DirectIntigrationActivity.this);
            }
        });

  ////show Rewarded Ad
       bt_Rewarded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mYodo1Mas.isRewardedAdLoaded())
             mYodo1Mas.showRewardedAd(DirectIntigrationActivity.this);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        //////Show Banner on Top or Bottom. For bottom, use "Yodo1Mas.BannerBottom"
        int align = Yodo1Mas.BannerTop | Yodo1Mas.BannerHorizontalCenter;

        mYodo1Mas.showBannerAd(this, align);
    }
}