package com.zeetech.yodo1.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {


    Button directIntegration, throughUtility;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        directIntegration=findViewById(R.id.direct);
        throughUtility=findViewById(R.id.utility);



        /////select below one integration type which you want to integrate

        /////basic direct integration(without utility)
        directIntegration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,DirectIntigrationActivity.class));

            }
        });

        /////integration through utility
        throughUtility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ThroughUtilityIntigrationActivity.class));
            }
        });
    }
}