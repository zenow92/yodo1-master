package com.zeetech.yodo1.demo;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.yodo1.mas.Yodo1Mas;
import com.yodo1.mas.error.Yodo1MasError;
import com.yodo1.mas.helper.model.Yodo1MasAdBuildConfig;


public class Yodo1Utils {
  static   Yodo1Utils  mClassInstance;

    static AppCompatActivity context;


  static Yodo1Mas mYodo1Ms;

    public Yodo1Utils(AppCompatActivity context) {
 this.context = context;;
        initialize();
    }

    public static Yodo1Utils getInstance(AppCompatActivity context_activity) {

        if (mClassInstance==null){

            mClassInstance=new Yodo1Utils(context_activity);

            return mClassInstance;
        }

       else


        return mClassInstance;
    }
    public static void initialize(){

    mYodo1Ms= Yodo1Mas.getInstance();
       mYodo1Ms.init(context, context.getResources().getString(R.string.yodo_app_key), new Yodo1Mas.InitListener() {
            @Override
            public void onMasInitSuccessful() {

            }

            @Override
            public void onMasInitFailed(@NonNull Yodo1MasError error) {
            }
        });


    }
    public void showBanner(int pos){

            int align = pos | Yodo1Mas.BannerHorizontalCenter;

           // Yodo1MasAdBuildConfig config = new Yodo1MasAdBuildConfig.Builder().enableAdaptiveBanner(true).build();
          ///  Yodo1Mas.getInstance().setAdBuildConfig(config);


            mYodo1Ms.showBannerAd(context, align);
        }

    public void showAdaptiveBanner(int pos){

            int align = pos | Yodo1Mas.BannerHorizontalCenter;

        Yodo1MasAdBuildConfig config = new Yodo1MasAdBuildConfig.Builder().enableAdaptiveBanner(true).build();
        mYodo1Ms.setAdBuildConfig(config);


            mYodo1Ms.showBannerAd(context, align);
        }





    public void setOnInterstitialClickListener(Yodo1Mas.InterstitialListener interstitialListener)
    {

            mYodo1Ms.setInterstitialListener(interstitialListener);
    }

    public void setRewardedClickListener(Yodo1Mas.RewardListener rewardedClickListenerListener)
    {

            mYodo1Ms.setRewardListener(rewardedClickListenerListener);
    }





    public void showInterstitial()
    {

            mYodo1Ms.showInterstitialAd(context);

    }


    public boolean isInterstitialLoaded()
    {

            return mYodo1Ms.isInterstitialAdLoaded();

    }

    public void showRewarded()
    {

        mYodo1Ms.showRewardedAd(context);

    }


    public boolean isRewardedLoaded()
    {

        boolean isLoaded = Yodo1Mas.getInstance().isRewardedAdLoaded();
        return isLoaded;

    }



}
