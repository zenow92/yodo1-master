# Yodo1-master

Yodo1 Mas ads integration demo for android

## Getting started

To make it easy for you to get started with Yodo1 for android, here's a list of recommended  steps.

## Create Yodo1 Mas Account(Register Your self)
https://mas.yodo1.com/register?referral_code=RP_VXDN9IF

**Integration Guide:**
> To test ad on this demo, Change its package name & get new keys after adding new test app in Yodo1 Dashboard against this new package name

1 >> Add (App key) and (Yodo Admob App id) in string.xml. You can get these 2 keys from Yodo1 dashboard after adding new Game/App.

https://mas.yodo1.com/register?referral_code=RP_VXDN9IF

2 >> Add dependence in build.gradle(app-level).

```
 dependencies {
   /////////yodo1 dependence
     implementation 'com.yodo1.mas:full:4.3.1'
 
 }
```

 

3 >> Add maven in build.gradle(Project-level.


```
 allprojects {
    repositories 
    {
        google()
        jcenter()
        mavenCentral()
  
        ////////add below for Yodo1
        maven { url "https://artifact.bytedance.com/repository/pangle" }
       maven { url "https://android-sdk.is.com" }
        maven { url "https://sdk.tapjoy.com/" }

    }
} 
```
  4 >> Add meta data in manifest.xml


```
    ///////////////////////////Yodo1 Admob App id (get it from Yodo1 Mas Dashboard, unique for each app And game)
        <!-- Sample AdMob App ID: ca-app-pub-3940256099942544~3347511713 -->
        <meta-data
            android:name="com.google.android.gms.ads.APPLICATION_ID"
            android:value="@string/yodo_admob_app_id" />
            
```


5 >> Also Add test device on yodo1 dashboard to test ads.

**6 >> Note:If you got an error like "unknown host android-sdk.is.com" or "irson source error" in building project change your internet connection to mobile hotspot or some other network.**


>>> Feel free to contact us on zenow92@gmail.com if you face any issue. 

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/zenow92/yodo1-master.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:1ae7f06e04b22280ec43e4e65cb2c03c?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

